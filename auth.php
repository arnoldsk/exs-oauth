<?php

class Auth
{
    private $id = null;
    private $password = null;
    private $user = null;
    private $error = false;

    public function __construct()
    {
        try {
            $this->parsePostData();
            $this->parseUserId();
            $this->authenticate();

            $this->dataResponse();
        } catch (Exception $e) {
            $this->error = $e->getMessage();

            $this->errorResponse();
        }
    }

    private function parsePostData()
    {
        $this->id = isset($_POST['id']) ? $_POST['id'] : null;
        $this->password = isset($_POST['password']) ? $_POST['password'] : null;

        if (!$this->id || !$this->password) {
            throw new Exception('ID un/vai parole netika nosūtīta');
        }
    }

    private function parseUserId()
    {
        $url = 'https://exs.lv/user_stats/json/' . $this->id;
        $json = file_get_contents($url);
        $data = json_decode($json);

        $this->user = $data;

        if (!$this->user) {
            throw new Exception('Lietotājs netika atrasts');
        }
    }

    private function authenticate()
    {
        $url = 'https://android.exs.lv/auth/login';
        $fields = http_build_query([
            'username' => $this->user->nick,
            'password' => $this->password,
        ]);

        $req = curl_init();

        curl_setopt($req, CURLOPT_URL, $url);
        curl_setopt($req, CURLOPT_POST, 1);
        curl_setopt($req, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($req, CURLOPT_RETURNTRANSFER, 1);

        $res = curl_exec($req);
        $res = json_decode($res, true);

        curl_close($req);

        if (!(isset($res['state']) && $res['state'] == 'success')) {
            throw new Exception('Nepareiza lietotāja parole');
        }
    }

    private function dataResponse()
    {
        $response = [
            'data' => $this->user,
        ];

        $this->sendResponse($response);
    }

    private function errorResponse()
    {
        $response = [
            'error' => $this->error,
        ];

        $this->sendResponse($response);
    }

    private function sendResponse($response)
    {
        header('Content-Type: application/json');
        echo json_encode($response);
        die;
    }
}

$auth = new Auth();
