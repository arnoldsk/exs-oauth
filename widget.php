<?php

$config = require 'config.php';

$url = $config['url'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Exs oAuth</title>

    <style>
        * {
            box-sizing: border-box;
        }

        body {
            font-family: sans-serif;
            font-size: 18px;
            margin: 20px auto;
            width: 300px;
        }

        .form-item {
            margin: 10px 0 20px;
        }

        .form-label {
            display: inline-block;
            margin-bottom: 10px;
            padding-left: 10px;
            cursor: pointer;
        }

        .form-input {
            width: 100%;
            padding: 20px;
            border-radius: 10px;
            border: 1px solid #ddd;
            outline: none;
        }

        .form-button {
            padding: 20px;
            background: #2ecc71;
            border-radius: 10px;
            border: none;
            outline: none;
            color: #fff;
            text-transform: uppercase;
            cursor: pointer;
            transition: 100ms ease-in-out;
        }

        .form-button:active {
            opacity: 0.8;
        }

        .form-button[disabled] {
            background: #aaa;
        }
    </style>
</head>
<body>
	<h1>Exs OAuth</h1>

    <form id="form" action="javascript:void(0)" autocomplete="off">
        <div class="form-item">
            <label class="form-label" for="id">Lietotāja ID</label>
            <input id="id" class="form-input" type="number" min="0" name="id" required>
        </div>
        <div class="form-item">
            <label class="form-label" for="password">Lietotāja parole</label>
            <input id="password" class="form-input" type="password" name="password" required>
        </div>

        <div class="form-item">
            <button class="form-button" type="submit">Autentificēt</button>
        </div>
    </form>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script>
        $(document).on('keydown', (e) => {
            if ((e.which || e.keyCode) == 116 || (e.which || e.keyCode) == 82) {
                e.preventDefault();
            }
        });

        $(document).on('submit', '#form', async () => {
            const getFields = () => {
                const data = $('#form').serializeArray();
                const fields = {};

                for (const item of data) {
                    fields[item.name] = item.value;
                }

                return fields;
            };
            const finish = () => {
                window.close();
            };

            try {
                $('.form-button').prop('disabled', true);

                const auth = await $.post('<?= $url ?>auth.php', getFields());

                $('body')[0].outerHTML = JSON.stringify(auth);

                finish();
            } catch(error) {

                finish();
            }
        });
    </script>
</body>
</html>
