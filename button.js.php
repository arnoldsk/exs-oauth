<?php

$config = require 'config.php';

$url = $config['url'];

header('Content-Type: application/javascript');

?>

class ExsOAuth {
    constructor(callback, options = {}) {
        if (typeof callback !== 'function') {
            throw new Error('The first constructor parameter has to be a function!');
        }

        this.callback = callback;

        this.setOptions(options);
        this.render();
    }

    setOptions(options) {
        this.options = {};

        if (typeof options !== 'object') {
            return false;
        }

        for (const [key, value] of Object.entries(options)) {
            this.options[key] = value;
        }

        return true;
    }

    getOption(key, fallback = null) {
        if (Object.keys(this.options).indexOf(key) !== -1) {
            return this.options[key];
        }

        return fallback;
    }

    safeParseJson(json) {
        try {
            return JSON.parse(json);
        } catch(error) {
            return null;
        }
    }

    render() {
        const buttonId = this.getOption('buttonId', 'exs-oauth');
        const element = document.getElementById(buttonId);

        if (element !== null) {
            const buttonClass = this.getOption('buttonClass', '');

            element.innerHTML = `<button id="${buttonId}-button" class="${buttonClass}" type="button">Ienākt ar Exs OAuth</button>`;

            const button = document.getElementById(`${buttonId}-button`);
            const windowUrl = '<?= $url ?>widget';

            button.addEventListener('click', () => {
                if (window.ExsOAuth && !window.ExsOAuth.closed) {
                    window.ExsOAuth.close();
                }

                const ExsOAuthWindow = window.open(windowUrl, 'ExsOAuth', 'width=320,height=400');

                window.ExsOAuth = ExsOAuthWindow;

                ExsOAuthWindow.onbeforeunload = () => {
                    const content = ExsOAuthWindow.document.body.innerHTML;
                    const result = this.safeParseJson(content);

                    result && this.callback(result);
                };
            });
        }
    }
}
